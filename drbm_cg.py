import cPickle
import gzip
import os
import sys
import time

import numpy as np

import theano
import theano.tensor as T

"""Implementation of the discriminative RBM trained with conjugate gradients in
Theano.
"""

class DiscriminativeRBM(object):
    """Discriminative restricted Boltzmann machine class

    The discriminative RBM is fully characterized by four parameters:
      * Weight matrix W between input units in the visible layer and the hidden
        units.
      * Weight matrix U between output units in the visible layer and the
        hidden units.
      * Bias vector c of the hidden layer units.
      * Bias vector d of the output units.

    Classification is done using the class-wise posterior probabilities
    predicted by the model in its output units.    
    """

    def __init__(self, input, n_inputs, n_classes, n_hiddens,
                 weight_decay=1e-4, rng=0):
        """ Initialize the parameters of the DRBM 

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
            architecture (one minibatch)

        :type n_inputs: int
        :param n_inputs: number of input units, the dimension of the space in
            which the datapoints lie

        :type n_classes: int
        :param n_outputs: number of output units, the dimension of the space in
            which the labels lie

        :type n_hiddens: int
        :param n_hiddens: number of hidden units, the dimension of the
            latent-variable space 

        :weight_decay: float
        :param weight_decay: weight decay regularisation for stochastic
            gradient descent
        """
        # Model parameters
        self.n_inputs=n_inputs
        self.n_classes=n_classes
        self.n_hiddens=n_hiddens
        self.weight_decay=weight_decay
        self.rng=rng

        # initialize theta = (U,W,c,d) with 0s; U gets the shape (n_classes,
        # n_hiddens), W gets the shape (n_inputs, n_hiddens), while c is a
        # vector of n_hiddens elements and d is a vector of n_classes elements,
        # making theta a vector of n_inputs*n_hiddens + n_classes*n_hiddens +
        # n_classes + n_hiddens  elements
        theta_values = np.asarray(
                np.concatenate(
                    ((rng.rand(n_classes*n_hiddens,)*2-1)*(1/np.sqrt(max(n_classes,
                                                                         n_hiddens))),
                     (rng.rand(n_inputs*n_hiddens,)*2-1)*(1/np.sqrt(max(n_inputs,
                                                                        n_hiddens))),
                     np.zeros((n_classes+n_hiddens,))), axis=0), 
                dtype=theano.config.floatX)
        self.theta = theano.shared(value=theta_values, name='theta',
                                   borrow=True)

        # initialize U
        s_idx = 0
        e_idx = n_classes * n_hiddens
        self.U = self.theta[s_idx:e_idx].reshape((n_classes, n_hiddens))
        
        # initialize W
        s_idx = e_idx
        e_idx = s_idx + n_inputs*n_hiddens
        self.W = self.theta[s_idx:e_idx].reshape((n_inputs, n_hiddens))

        # initialize c
        s_idx = e_idx 
        e_idx = s_idx + n_hiddens
        self.c = self.theta[s_idx:e_idx]

        # initialize d
        s_idx = e_idx
        e_idx = s_idx + n_classes
        self.d = self.theta[s_idx:e_idx]
        
        # L2-regularizer
        self.L2 = (self.W**2).sum()

        # posterior probability
        self.p_y_given_x = self.p_y_given_x_fn(input)

        # compute prediction as class whose probability is maximal in
        # symbolic form
        self.y_pred = theano.tensor.argmax(self.p_y_given_x, axis=1)


    def p_y_given_x_fn(self, X):
        """Return the posterior probability of classes given inputs and model
        parameters (p(y|x, theta)).
       
        Input
        -----
        :type X: theano.shared
        :param X: Input data matrix

        Output
        ------
        :type p: ???
        :param p: Posterior class probabilities of the input data points as
            given by the model 
        """ 
        # NOTE: Doing this for now as theano.scan returns an error when passing
        # class variables directly into it.
        U = self.U
        W = self.W
        c = self.c
        d = self.d
        Y_class = theano.shared(np.eye(self.n_classes,
                                          dtype=theano.config.floatX),
                                name='Y_class') 

        # Compute hidden states
        s_hid = theano.tensor.dot(X, W) + c

        # Compute energies
        energies, updates = theano.scan(lambda y_class, U, s_hid: 
                                        s_hid + theano.tensor.dot(y_class, U),
                                        sequences=[Y_class],
                                        non_sequences=[U, s_hid])

        # Compute log-posteriors
        log_p, updates = theano.scan(
                lambda d_i, e_i: d_i + \
                        theano.tensor.sum(theano.tensor.log(1+theano.tensor.exp(e_i)),
                                          axis=1),
                 sequences=[d, energies], non_sequences=[])
        log_p = log_p.T # TODO: See if this can be avoided

        # Compute unnormalized posteriors
        unnorm_p = theano.tensor.exp(log_p - theano.tensor.max(log_p, axis=1,
                                                               keepdims=True))

        # Compute posteriors
        p = unnorm_p / theano.tensor.sum(unnorm_p, axis=1, keepdims=True)

        return p


    def negative_log_likelihood(self, y):
        """Return the negative log-likelihood of the prediction of this model
        under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|}\mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|}\sum_{i=0}^{|\mathcal{D}|} \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
                \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example
                  the correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred.ndim:
            raise TypeError('y should have the same shape as self.y_pred',
                ('y', target.type, 'y_pred', self.y_pred.type))
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.neq(self.y_pred, y))
        else:
            raise NotImplementedError()


def load_data(dataset):
    ''' Loads the dataset

    :type dataset: string
    :param dataset: the path to the dataset (here MNIST)
    '''

    #############
    # LOAD DATA #
    #############

    # Download the MNIST dataset if it is not present
    data_dir, data_file = os.path.split(dataset)
    if data_dir == "" and not os.path.isfile(dataset):
        # Check if dataset is in the data directory.
        new_path = os.path.join(os.path.split(__file__)[0], "..", "data", dataset)
        if os.path.isfile(new_path) or data_file == 'mnist.pkl.gz':
            dataset = new_path

    if (not os.path.isfile(dataset)) and data_file == 'mnist.pkl.gz':
        import urllib
        origin = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        print 'Downloading data from %s' % origin
        urllib.urlretrieve(origin, dataset)

    print '... loading data'

    # Load the dataset
    f = gzip.open(dataset, 'rb')
    train_set, valid_set, test_set = cPickle.load(f)
    f.close()
    #train_set, valid_set, test_set format: tuple(input, target)
    #input is an np.ndarray of 2 dimensions (a matrix)
    #witch row's correspond to an example. target is a
    #np.ndarray of 1 dimensions (vector)) that have the same length as
    #the number of rows in the input. It should give the target
    #target to the example with the same index in the input.

    def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(np.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(np.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval


def cg_optimization_mnist(n_hiddens=50, weight_decay=1e-4, n_epochs=50,
                          dataset='mnist.pkl.gz', batch_size=600): 
    """Demonstrate stochastic gradient descent optimization of a Discriminative
    restricted Boltzmann machine.

    The default dataset for the demonstration is MNIST.

    :type weight_decay: float
    :param weight_decay: weight-decay for regularization

    :type n_epochs: int
    :param n_epochs: number of epochs to run the optimizer

    :type dataset: string
    :param dataset: the path of the dataset file in the same format as the
                    MNIST dataset available at
                 http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz

    :type batch_size: int
    :param batch_size: size of data-batches used for training
    """
    #############
    # LOAD DATA #
    #############
    datasets = load_data(dataset)

    train_set_x, train_set_y = datasets[0]
    valid_set_x, valid_set_y = datasets[1]
    test_set_x, test_set_y = datasets[2]

    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] / batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] / batch_size

    # obtain number of inputs and outputs for the model
    n_inputs = valid_set_x.get_value().shape[1]
    n_classes = np.shape(np.unique(np.concatenate((train_set_y.eval(), 
                                                   test_set_y.eval(),
                                                   valid_set_y.eval()), 
                                                   axis=0)))[0]

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print '... building the model'

    # allocate symbolic variables for the data
    minibatch_offset = T.lscalar()  # offset to the start of a [mini]batch
    x = T.matrix()   # the data is presented as rasterized images
    y = T.ivector()  # the labels are presented as 1D vector of
                     # [int] labels

    rng = np.random.RandomState(666)

    # construct the DRBM class
    classifier = DiscriminativeRBM(input=x, n_inputs=n_inputs,
                                   n_classes=n_classes, n_hiddens=n_hiddens,
                                   weight_decay=weight_decay, rng=rng)

    # the cost we minimize during training is the negative log likelihood of
    # the model in symbolic format
    cost = classifier.negative_log_likelihood(y).mean() + \
            classifier.weight_decay * classifier.L2

    # compile a theano function that computes the mistakes that are made by
    # the model on a minibatch
    test_model = theano.function([minibatch_offset], classifier.errors(y),
            givens={
                x: test_set_x[minibatch_offset:minibatch_offset + batch_size],
                y: test_set_y[minibatch_offset:minibatch_offset + batch_size]},
            name="test")

    validate_model = theano.function([minibatch_offset], classifier.errors(y),
            givens={
                x: valid_set_x[minibatch_offset:
                               minibatch_offset + batch_size],
                y: valid_set_y[minibatch_offset:
                               minibatch_offset + batch_size]},
            name="validate")

    #  compile a theano function that returns the cost of a minibatch
    batch_cost = theano.function([minibatch_offset], cost,
            givens={
                x: train_set_x[minibatch_offset:
                               minibatch_offset + batch_size],
                y: train_set_y[minibatch_offset:
                               minibatch_offset + batch_size]},
            name="batch_cost")

    # compile a theano function that returns the gradient of the minibatch
    # with respect to theta
    batch_grad = theano.function([minibatch_offset],
                                 T.grad(cost, classifier.theta),
                                 givens={
                                     x: train_set_x[minibatch_offset:
                                            minibatch_offset + batch_size],
                                     y: train_set_y[minibatch_offset:
                                            minibatch_offset + batch_size]},
            name="batch_grad")

    # creates a function that computes the average cost on the training set
    def train_fn(theta_value):
        classifier.theta.set_value(theta_value, borrow=True)
        train_losses = [batch_cost(i * batch_size)
                        for i in xrange(n_train_batches)]
        return np.mean(train_losses)

    # creates a function that computes the average gradient of cost with
    # respect to theta
    def train_fn_grad(theta_value):
        classifier.theta.set_value(theta_value, borrow=True)
        grad = batch_grad(0)
        for i in xrange(1, n_train_batches):
            grad += batch_grad(i * batch_size)
        return grad / n_train_batches

    validation_scores = [np.inf, 0]

    # creates the validation function
    def callback(theta_value):
        classifier.theta.set_value(theta_value, borrow=True)
        #compute the validation loss
        validation_losses = [validate_model(i * batch_size)
                             for i in xrange(n_valid_batches)]
        this_validation_loss = np.mean(validation_losses)
        print('validation error %f %%' % (this_validation_loss * 100.,))

        # check if it is better then best validation score got until now
        if this_validation_loss < validation_scores[0]:
            # if so, replace the old one, and compute the score on the
            # testing dataset
            validation_scores[0] = this_validation_loss
            test_losses = [test_model(i * batch_size)
                           for i in xrange(n_test_batches)]
            validation_scores[1] = np.mean(test_losses)

    ###############
    # TRAIN MODEL #
    ###############
#    theta_init = np.asarray(
#            np.concatenate(
#                (rng.rand((n_classes+n_inputs)*n_hiddens,)*1e-3,
#                 np.zeros((n_classes+n_hiddens,))), axis=0), 
#            dtype=x.dtype)
    theta_init = np.asarray(
            np.concatenate(
                ((rng.rand(n_classes*n_hiddens,)*2-1)*(1/np.sqrt(max(n_classes,
                                                                     n_hiddens))),
                 (rng.rand(n_inputs*n_hiddens,)*2-1)*(1/np.sqrt(max(n_inputs,
                                                                    n_hiddens))),
                 np.zeros((n_classes+n_hiddens,))), axis=0), 
            dtype=theano.config.floatX)
 

    # using scipy conjugate gradient optimizer
    import scipy.optimize
    print ("Optimizing using scipy.optimize.fmin_cg...")
    start_time = time.clock()
    best_w_b = scipy.optimize.fmin_cg(
               f=train_fn,
               x0=theta_init,
               fprime=train_fn_grad,
               callback=callback,
               disp=0,
               maxiter=n_epochs)
    end_time = time.clock()
    print(('Optimization complete with best validation score of %f %%, with '
          'test performance %f %%') %
               (validation_scores[0] * 100., validation_scores[1] * 100.))

    print >> sys.stderr, ('The code for file ' +
                          os.path.split(__file__)[1] +
                          ' ran for %.1fs' % ((end_time - start_time)))


if __name__ == '__main__':
    cg_optimization_mnist(n_hiddens=500, n_epochs=1000)
