# Theano DRBM #

An implementation of the Discriminative RBM in Python using Theano.

### Summary ###

* This repository contains an implementation of the Discriminative restricted Boltzmann machine [1] in [Theano](http://deeplearning.net/software/theano/) [2].
* The model can be trained using either stochastic gradient descent (SGD) or conjugate gradients (CG).
* The code for computing the model posteriors is based on a MATLAB implementation of the model in the [PRTools](http://www.37steps.com/prtools/) pattern recognition toolbox.
* The code for class-definitions, dataset IO, SGD and CG optimizations, etc. uses the [logistic_sgd.py](http://deeplearning.net/tutorial/code/logistic_sgd.py) and [logistic_cg.py](http://deeplearning.net/tutorial/code/logistic_cg.py) Theano examples.

### Execution ###

* Make sure the MNIST dataset file (available for download [here](http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz)) is in the same folder as the scripts.
* To train the DRBM using stochastic gradient descent, run 
```
$ python drbm_sgd.py
```
* To train the DRBM using conjugate gradients, run
```
$ python drbm_cg.py
```

### Performance ###

* I was able to reproduce the test error (within the bounds of statistical significance) reported in [1] using this model on the MNIST dataset by training it with CG for up to 1000 iterations. The log of this can be found in the file *checks/output.log.cg.1000*. The model has 500 hidden units.

* The test error reported in the paper was, however, using SGD with a learning rate of 0.05 and 500 hidden units which I haven't still reproduced. If anyone using this code achieves this, please share the log, and details of your method with me. The log of a test run with SGD (upto 2000 iterations, and with the same hyperparameters) can be found in *checks/output.log.sgd.2000*.

### References ###

* [1] Larochelle, H., & Bengio, Y. (2008, July). Classification using discriminative restricted Boltzmann machines. In Proceedings of the 25th international conference on Machine learning (pp. 536-543). ACM.
* [2] Bergstra, J., Breuleux, O., Bastien, F., Lamblin, P., Pascanu, R., Desjardins, G., ... & Bengio, Y. (2010, June). Theano: a CPU and GPU math compiler in Python. In Proc. 9th Python in Science Conf (pp. 1-7).

### Acknowledgements ###

Many thanks to Laurens van der Maaten whose MATLAB implementation of the DRBM I found to be a useful reference while writing this code, and to the development team of Theano for making it very easy to get started with the module.

### Contact ###

If you find any bugs, or would like to suggest improvements, please submit an issue or email me.

* Srikanth Cherla (abfb145_at_city_dot_ac_dot_uk)